import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH2 = styled.h2``;

export function H2({ children, ...props }) {
  return <StyledH2 {...props}>{children}</StyledH2>;
}

H2.propTypes = {
  children: propTypes.any,
};

H2.defaultProps = {
  children: null,
};
