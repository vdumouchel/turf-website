import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH3 = styled.h3``;

export function H3({ children, ...props }) {
  return <StyledH3 {...props}>{children}</StyledH3>;
}

H3.propTypes = {
  children: propTypes.any,
};

H3.defaultProps = {
  children: null,
};
