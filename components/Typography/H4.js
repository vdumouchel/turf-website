import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH4 = styled.h4``;

export function H4({ children, ...props }) {
  return <StyledH4 {...props}>{children}</StyledH4>;
}

H4.propTypes = {
  children: propTypes.any,
};

H4.defaultProps = {
  children: null,
};
