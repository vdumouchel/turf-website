import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledP = styled.p``;

export function P({ children, ...props }) {
  return <StyledP {...props}>{children}</StyledP>;
}

P.propTypes = {
  children: propTypes.any,
};

P.defaultProps = {
  children: null,
};
