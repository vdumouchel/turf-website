import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH1 = styled.h1``;

export function H1({ children, ...props }) {
    return <StyledH1 {...props}>{children}</StyledH1>;
}

H1.propTypes = {
    children: propTypes.any
};

H1.defaultProps = {
    children: null
};
