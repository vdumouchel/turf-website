import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH6 = styled.h6``;

export function H6({ children, ...props }) {
  return <StyledH6 {...props}>{children}</StyledH6>;
}

H6.propTypes = {
  children: propTypes.any,
};

H6.defaultProps = {
  children: null,
};
