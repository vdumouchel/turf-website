import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

const StyledH5 = styled.h5``;

export function H5({ children, ...props }) {
  return <StyledH5 {...props}>{children}</StyledH5>;
}

H5.propTypes = {
  children: propTypes.any,
};

H5.defaultProps = {
  children: null,
};
