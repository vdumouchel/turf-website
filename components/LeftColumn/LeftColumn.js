import styled from 'styled-components';
import { Logo } from '../../public/Logo';
import { Text } from '../Text';
import { H1, H2, H3, H4, P } from '../Typography';
import { colors } from '../../styles';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    max-width: 50vw;
`;

const LogoWrapper = styled.div`
    max-width: 10rem;
`;

const HeroTextHeading = styled(H1)`
    font-size: 2.8em;
    font-weight: 600;
    margin-bottom: 0rem;
    color: ${colors.white};
`;

const HeroTextSubHeading = styled(H2)`
    font-size: 1.8em;
    font-weight: 400;
    margin-bottom: 0.5rem;
    color: ${colors.white};
`;

const HeroTextParagraph = styled(H3)`
    font-size: 1.25em;
    font-weight: 400;
    margin-bottom: 0.5rem;
`;

export function LeftColumn(props) {
    return (
        <Wrapper>
            <LogoWrapper>
                <Logo color="white" />
            </LogoWrapper>
            <HeroTextHeading>Référez simplement vos patients.</HeroTextHeading>
            <HeroTextSubHeading>
                Trouvez rapidement un consultant parmi notre vaste répertoire de médecins québécois,
                peu importe la spécialité souhaitée. Référez-leur enfin votre patient directement et
                obtenez leur plan de traitement dans les plus brefs délais. Suivez leur évolution au
                bout de vos doigts.
            </HeroTextSubHeading>
        </Wrapper>
    );
}
