import styled from 'styled-components';
import { Logo } from '../../public/Logo';
import { AppStore } from '../../public/AppStore';
import { Text } from '../Text';
import { H1, H2, H3, H4, P } from '../Typography';
import { colors } from '../../styles';

const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: auto;
    min-height: 100vh;
    background: linear-gradient(
        180deg,
        #49767b -16.42%,
        #4c8c83 -3.95%,
        #57b596 9.91%,
        #5cc6a1 28.6%,
        #62d5aa 49.38%,
        #5ed1a8 61.2%,
        #5bc09e 75.22%,
        #4fa28b 93.01%,
        #4b8882 106.87%,
        #49767b 116.57%
    );

    @media (max-width: 767px) {
        flex-direction: column;
        min-height: 100vh;
    }
`;

const BodyWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 2rem;

    @media (max-width: 767px) {
        margin-top: 3rem;
        flex-direction: column;
    }
`;

const LeftColumn = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 2rem 0 4rem;
    align-content: center;

    @media (max-width: 767px) {
        margin: 0 0.5rem 0 0.5rem;
        max-width: 90vw;
    }
`;

const RightColumn = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 6rem 0 4rem;

    @media (max-width: 767px) {
        margin: 0 0.5rem 0 0.5rem;
    }
`;

const LogoWrapper = styled.div`
    max-width: 10rem;

    @media (max-width: 767px) {
        max-width: 9rem;
    }
`;

const HeroTextHeading = styled(H1)`
    font-size: 2.8em;
    font-weight: 600;
    margin-bottom: 0rem;
    color: ${colors.white};

    @media (max-width: 640px) {
        font-size: 2.3em;
    }
`;

const HeroTextSubHeading = styled(H2)`
    font-size: 1.8em;
    font-weight: 400;
    margin-bottom: 0.5rem;
    color: ${colors.white};

    @media (max-width: 767px) {
        font-size: 1.4em;
    }
`;

const HeroTextParagraph = styled(H3)`
    font-size: 1.25em;
    font-weight: 400;
    margin-bottom: 0.5rem;
`;

const ButtonsWrapper = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 2rem;

    @media (max-width: 767px) {
        flex-direction: column;
    }
`;

const AppStoreWrapper = styled.div`
    cursor: pointer;
`;

const ContactWrapper = styled.button`
    display: flex;
    width: 238px;
    height: 75px;
    margin-left: 1rem;
    background-color: #49767b;
    border: none;
    border-radius: 12px;
    justify-content: center;
    transition: ease 0.5s;
    cursor: pointer;

    &:hover {
        background-color: #66a197;
    }

    @media (max-width: 767px) {
        margin-left: 0rem;
        margin-top: 0.4rem;
        width: 238px;
    }
`;
const ContactText = styled(H2)`
    color: ${colors.white};
    text-align: center;
    vertical-align: middle;
    align-self: center;
`;

const ImgWrapper = styled.div`
    min-height: 500px;
`;

const Img = styled.img`
    max-width: 25vw;

    @media (max-width: 767px) {
        max-width: 85vw;
        min-height: 500px;
        margin-top: 3rem;
    }
`;

const FooterWrapper = styled.div`
    display: flex;
    flex-direction: row;
    margin-left: 4rem;
    margin-top: 3rem;
    align-self: flex-start;
    padding: auto;

    @media (max-width: 767px) {
        margin-left: 1rem;
    }
`;

const Footer = styled(P)`
    color: ${colors.white};
`;

export function Body(props) {
    return (
        <PageWrapper>
            <BodyWrapper>
                <LeftColumn>
                    <LogoWrapper>
                        <Logo color="white" />
                    </LogoWrapper>
                    <HeroTextHeading>Référez simplement vos patients.</HeroTextHeading>
                    <HeroTextSubHeading>
                        Trouvez rapidement un consultant parmi notre vaste répertoire de médecins
                        québécois, peu importe la spécialité souhaitée. Référez-leur enfin votre
                        patient directement et obtenez leur plan de traitement dans les plus brefs
                        délais. <br />
                        <br />
                        Suivez leur évolution au bout de vos doigts.
                    </HeroTextSubHeading>
                    <ButtonsWrapper>
                        <AppStoreWrapper>
                            <AppStore />
                        </AppStoreWrapper>
                        <ContactWrapper
                            onClick={() =>
                                window.open(
                                    `mailto:f.letarte@turfapp.ca?subject=Turf App - Demande d'information`,
                                    '_blank'
                                )
                            }>
                            <ContactText>Contactez-nous</ContactText>
                        </ContactWrapper>
                    </ButtonsWrapper>
                </LeftColumn>
                <RightColumn>
                    <ImgWrapper>
                        <Img src="/turf-iphone-consultations.png" alt="my image" />
                    </ImgWrapper>
                </RightColumn>
            </BodyWrapper>
            <FooterWrapper>
                <Footer> ©2020 Turf App. Tous droits réservés.</Footer>
            </FooterWrapper>
        </PageWrapper>
    );
}
