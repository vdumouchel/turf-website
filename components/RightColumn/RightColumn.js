import styled from 'styled-components';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;

const ImgWrapper = styled.div`
    max-width: 500px;
`;

export function RightColumn(props) {
    return (
        <Wrapper>
            <ImgWrapper>
                <img src="/turf- splashscreen- isometric.png" alt="my image" />
            </ImgWrapper>
        </Wrapper>
    );
}
