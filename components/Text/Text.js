import styled from 'styled-components';
import propTypes from 'prop-types';
import { text } from '../../styles';

export const Text = styled.span`
    ${(props) =>
        text(props.size || props.as || 'p', {
            weight: props.weight,
            muted: props.muted,
            spacing: props.spacing,
            shadow: props.shadow,
            uppercase: props.uppercase,
            color: props.color
        })}
`;

Text.propTypes = {
    /**
     * The dom node you want to render the Text as.
     */
    as: propTypes.string,
    /**
     * Color string for the the Text.
     */
    color: propTypes.string,
    /**
     * Wether or not the Text is going to appear muted.
     */
    muted: propTypes.bool,
    /**
     * The size you want to render the Text as.
     */
    size: propTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'p', 'small']),
    /**
     * Wether or not the Text is going to have text-shadow.
     */
    shadow: propTypes.bool,
    /**
     * Letter spacing to be applied on the Text.
     */
    spacing: propTypes.oneOf(['small', 'medium', 'large']),
    /**
     * Font weight to set on the Text.
     */
    weight: propTypes.oneOf(['light', 'normal', 'semibold', 'bold']),
    /**
     * Whether we should uppercase it
     */
    uppercase: propTypes.bool
};

Text.defaultProps = {
    color: 'inherit',
    muted: false,
    spacing: 'small',
    weight: 'normal',
    uppercase: false
};
