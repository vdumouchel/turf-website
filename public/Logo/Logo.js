import React from 'react';
import styled, { css } from 'styled-components';
import propTypes from 'prop-types';
import { transition } from '../../styles/variables';
import { colors } from '../../styles';

const Svg = styled.svg`
    width: 100%;
    transition: filter ${transition.normal} ease;

    * {
        transition: fill ${transition.normal} ease;
    }

    &:hover {
        /** Don't add hover effects to letter if it's unicolor */
        cursor: pointer;

        ${(props) =>
            !props.color ??
            css`
                .letters {
                    fill: ${colors.mint};
                }
            `}

        filter: brightness(0.875);
    }
`;

function Letters(props) {
    return (
        <g fill={props.color ?? colors.cosmos} className="letters">
            <rect x="152.106" y="63.3415" width="5.17073" height="12.9268" />
            <rect
                x="148.228"
                y="72.3902"
                width="5.17073"
                height="12.9268"
                transform="rotate(-90 148.228 72.3902)"
            />
            <path d="M40.8461 86.1007H21.9941V17.9756H0V0H62.8402V17.9756H40.8461V86.1007Z" />
            <path d="M77.4581 71.9566C81.1587 71.9566 85.348 71.2582 90.0261 69.8612V37.3824H108.355V86.1007H94.0664L90.3086 82.6083C86.3083 84.0635 84.4054 84.6339 80.9143 85.6816C77.493 86.6594 74.5954 87.1484 72.2214 87.1484H67.5084C62.7605 87.1484 58.9551 85.7165 56.0924 82.8528C53.2297 79.9891 51.7983 76.1824 51.7983 71.4328V37.3824H70.1267V68.2897C70.1267 69.3374 70.4758 70.2105 71.1741 70.909C71.8723 71.6074 72.7451 71.9566 73.7924 71.9566H77.4581Z" />
            <path d="M148.13 49.0187C143.38 49.0187 137.119 50.6301 137.119 56.2317C137.119 61.8333 137.119 86.1007 137.119 86.1007H118.791V37.3824H134.19L137.119 41.0154C142.675 35.5586 147.143 36.65 153.706 36.65H159.637V49.0187C159.637 49.0187 152.881 49.0187 148.13 49.0187Z" />
            <path d="M212 27.2255C209.975 27.0858 207.287 26.8763 203.936 26.5969C200.584 26.3175 197.686 26.1778 195.243 26.1778C192.799 26.1778 191.228 26.527 190.53 27.2255C189.831 27.924 189.482 28.797 189.482 29.8448V36.65H206.763V49.0187H189.482V86.4208H171.154V49.0187H163.822V36.65H171.154V27.7493C171.154 22.9998 172.585 19.1931 175.448 16.3294C178.311 13.4656 182.116 12.0338 186.864 12.0338C191.123 12.0338 195.627 12.1735 200.375 12.4529C205.123 12.7322 208.998 12.9418 212 13.0815V27.2255Z" />
        </g>
    );
}

export function Logo(props) {
    return (
        <Svg {...props} viewBox={`0 0 212 88`}>
            {!props.glyph && <Letters {...props} />}
        </Svg>
    );
}

Logo.propTypes = {
    /**
     * Color to for the whole logo if you want to display it only in one color.
     */
    color: propTypes.string,
    /**
     * Display logo as a glyph (icon) only or full logo.
     */
    glyph: propTypes.bool
};
Letters.propTypes = Logo.propTypes;

Logo.defaultProps = {
    glyph: false
};
