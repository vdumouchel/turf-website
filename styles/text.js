import { css } from 'styled-components';

export const fontSizes = {
    h1: css`
        font-size: 2.5rem; /* 40px */
        line-height: 3rem;
    `,
    h2: css`
        font-size: 2rem; /* 32px */
        line-height: 2.5rem;
    `,
    h3: css`
        font-size: 1.5rem; /* 24px */
        line-height: 1.875rem;
    `,
    h4: css`
        font-size: 1.25rem; /* 20px */
        line-height: 1.75rem;
    `,
    h5: css`
        font-size: 1.125rem; /* 18px; */
        line-height: 1.5rem;
    `,
    p: css`
        font-size: 1rem; /* 16px */
        line-height: 1.25rem;
    `,
    small: css`
        font-size: 0.8rem; /* 12.8px */
        line-height: 0.8rem;
    `
};

export const fontSpacings = {
    small: css`
        letter-spacing: 0.5px;
    `,
    medium: css`
        letter-spacing: 1px;
    `,
    large: css`
        letter-spacing: 1.5px;
    `
};

export const fontWeights = {
    light: css`
        font-weight: 300;
    `,
    normal: css`
        font-weight: 400;
    `,
    semibold: css`
        font-weight: 600;
    `,
    bold: css`
        font-weight: 700;
    `
};
