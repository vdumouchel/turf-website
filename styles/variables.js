export const transition = {
    veryFast: '0.15s',
    fast: '0.3s',
    normal: '0.5s',
    slow: '0.8s',
    snail: '1s'
};
