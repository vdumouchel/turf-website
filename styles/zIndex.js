/**
 * When a component interacts with another component, extract the z-index and put it here
 * Keep this file ordered by z-indexes please
 */

export const headerContainer = 1;

export const appNav = 5;

export const menuPopover = 10;

export const dialog = 30;

export const toast = 50;

// you shouldn't be using this, but it's there.
export const highest = 999;
