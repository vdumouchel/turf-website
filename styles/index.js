import * as colors from './palette';
import * as zIndexes from './zIndex';
import * as variables from './variables';
export * from './variables';
import { fontSizes, fontWeights, fontSpacings } from './text';
import { css } from 'styled-components';
import { rgba } from 'polished';

export function zIndex(name, offset = 0) {
    return zIndexes[name] + offset;
}

export function color(name, alpha = 1) {
    const val = (function () {
        if (colors[name]) {
            return colors[name];
        }

        return name;
    })();

    if (alpha === 1) {
        return val;
    }
    return rgba(val, alpha);
}

export function text(
    size,
    { weight, color: textcolor, spacing = 'small', muted, shadow, uppercase } = {}
) {
    return css`
        ${size && fontSizes[size]}
        ${weight && fontWeights[weight]}
    ${spacing && fontSpacings[spacing]}

    ${muted &&
        css`
            opacity: 0.8;
        `}
    ${shadow &&
        css`
            text-shadow: 0px 1px 0px ${color('cosmos')};
        `}

    ${uppercase &&
        css`
            text-transform: uppercase;
        `}
    ${textcolor &&
        css`
            color: ${color(textcolor)};
        `}
    `;
}

export { colors, variables };
