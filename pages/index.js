import Head from 'next/head';
import { Body } from '../components/Body';

export default function Layout(props) {
    return (
        <div>
            <Head>
                <title>Turf App</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta charSet="utf-8" />
            </Head>
            <Body />
            <style jsx global>{`
                * {
                    box-sizing: border-box;
                }

                html,
                body,
                #__next {
                    height: 100%;
                    width: 100%;
                }

                body {
                    margin: 0;
                    padding: 0;
                    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
                        'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
                        sans-serif;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                }

                .Layout {
                    display: flex;
                    flex-direction: column;
                    height: 100vh;
                    width: 100vw;
                    background: linear-gradient(
                        180deg,
                        #49767b -16.42%,
                        #4c8c83 -3.95%,
                        #57b596 9.91%,
                        #5cc6a1 28.6%,
                        #62d5aa 49.38%,
                        #5ed1a8 61.2%,
                        #5bc09e 75.22%,
                        #4fa28b 93.01%,
                        #4b8882 106.87%,
                        #49767b 116.57%
                    );
                }

                .Content {
                    flex: 1;
                    display: flex;
                    flex-direction: column;
                    font-family: Arial;
                }
            `}</style>
        </div>
    );
}
